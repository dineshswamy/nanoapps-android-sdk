package co.nanoapps.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import static co.nanoapps.android.Constants.NANOAPP_USAGE_DETAILS;
import static co.nanoapps.android.Constants.SERVICE_INTENT;
import static co.nanoapps.android.Nanoapps.nanoappUsageDetails;

public class NanoappActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    Date startTime, endTime;
    String nanoappId="";
    String TAG = "NanoappActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startTime = new Date();
        nanoappId = getIntent().getStringExtra("nanoapp_id");
        //Starting the job to check and add updates of the nanoapp
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job job = createUpdaterJob(dispatcher);
        Log.w(TAG, "Nanoapp job scheduled");
        dispatcher.mustSchedule(job);
    }

    public static Job createUpdaterJob(FirebaseJobDispatcher dispatcher) {
        Job job = dispatcher.newJobBuilder()
                //persist the task across boots
                .setLifetime(Lifetime.FOREVER)
                //.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                //call this service when the criteria are met.
                .setService(NanoappUpdaterJobService.class)
                //unique id of the task
                .setTag("NanoappUpdaterService")
                //don't overwrite an existing job with the same tag
                .setReplaceCurrent(false)
                // We are mentioning that the job is periodic.
                .setRecurring(false)
                // Run between 30 - 60 seconds from now.
                .setTrigger(Trigger.executionWindow(5, 10))
                // retry with exponential backoff
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                //.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                //Run this job only when the network is available.
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        return job;
    }

    public static String current_component_name="";
    @Override
    protected String getMainComponentName() {
        Log.w(TAG, "Main component name" + current_component_name);
        return current_component_name;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        endTime = new Date();
        JSONObject usageDetail = new JSONObject();
        try {
            usageDetail.put("nanoapp_id",Integer.parseInt(nanoappId));
            usageDetail.put("start_time", Utils.parseDate(startTime));
            usageDetail.put("end_time", Utils.parseDate(endTime));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nanoappUsageDetails.put(usageDetail);
        Intent intent = new Intent(this, NanoappsUpdaterService.class);
        intent.putExtra(SERVICE_INTENT, NANOAPP_USAGE_DETAILS);
        intent.putExtra("details", nanoappUsageDetails.toString());
        intent.putExtra("project_token", Nanoapps.projectToken);
        this.startService(intent);
        nanoappUsageDetails = new JSONArray();
    }
}