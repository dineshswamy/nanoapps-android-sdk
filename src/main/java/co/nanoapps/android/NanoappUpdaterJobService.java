package co.nanoapps.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static co.nanoapps.android.Constants.DEVICE_AUTH_TOKEN;
import static co.nanoapps.android.Constants.NANOAPPS_ASSETS_URL;
import static co.nanoapps.android.Constants.X_NANOAPP_DEVICE_TOKEN;
import static co.nanoapps.android.Nanoapps.context;

/**
 * Created by dineshswamy on 2/11/18.
 */

public class NanoappUpdaterJobService extends JobService {
    NanoappsDatabaseHelper nanoappsDatabaseHelper;
    public static String TAG = "JobService";
    @Override
    public boolean onStartJob(JobParameters params) {
        //Call api for checking for any updates.
        nanoappsDatabaseHelper = NanoappsDatabaseHelper.getInstance(this);
        getUpdateDetails();
        return false;
    }

    private void getUpdateDetails() {
        Log.w(TAG, "NanoappUpdater job started");
        JSONArray packageNames = nanoappsDatabaseHelper.getAllPackages();
        String url = Constants.NANOAPPS_API_URL+"/api/nanoapp/get_nanoapp_update_details";
        SharedPreferences _prefs = context.getSharedPreferences(Constants.NANOAPP_PREFERENCES,
                Context.MODE_PRIVATE);
        final String x_nanoapp_device_token = "Bearer "+_prefs.getString(DEVICE_AUTH_TOKEN,"");
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, url,
                packageNames, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    Log.w(TAG, "Json array recieved" + response.toString());
                    for (int i = 0; i < response.length();i++) {
                        JSONObject nanoappJsonObject = response.getJSONObject(i);
                        Nanoapp nanoapp = new Nanoapp(nanoappJsonObject);
                        updateNanoapp(nanoapp);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.w(TAG, e.getLocalizedMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put(X_NANOAPP_DEVICE_TOKEN,x_nanoapp_device_token);
                return params;
            }
        };
        Log.w(TAG, jsonArrayRequest.getUrl());
        Nanoapps.getRequestQueue().add(jsonArrayRequest);
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private void updateNanoapp(final Nanoapp nanoapp) {
        Log.w(TAG, "Downloading the bundle");
        String url = NANOAPPS_ASSETS_URL + nanoapp.bundle_url;
        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, url, new Response.Listener<byte[]>() {
            @Override
            public void onResponse(byte[] response) {
                try {
                    if (response != null) {
                        File folder = new File(nanoapp.getAbsoluteFolder());
                        if (!folder.exists()) {
                            folder.mkdirs();
                        }
                        File bundleFile = new File(folder.getAbsolutePath(), "index.android.js");
                        bundleFile.createNewFile();
                        FileOutputStream outputStream;
                        outputStream = new FileOutputStream(bundleFile, false);
                        outputStream.write(response);
                        outputStream.close();
                        //Verify current_checksum of the file downloaded
                        if (Utils.verifyChecksum(bundleFile, nanoapp.current_checksum)){
                            nanoappsDatabaseHelper.addOrUpdateNanoapp(nanoapp);
                            Log.w(TAG, "Downloading the bundle complete");
                        } else {
                            Log.w("Nanoapp", "Checksum Error Occurred");
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {

            }
        }, null);
        RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        mRequestQueue.add(request);
    }
}

