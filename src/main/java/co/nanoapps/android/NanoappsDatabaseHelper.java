package co.nanoapps.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dineshswamy on 2/10/18.
 */

public class NanoappsDatabaseHelper extends SQLiteOpenHelper {
    // Database Info
    private static final String DATABASE_NAME = "nanoappsDatabase";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = "NanoappsDatabaseHelper";

    // Table Names
    private static final String TABLE_NANOAPPS = "nanoapps";

    // Nanoapps Table Columns
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String IMAGE_URL = "image_url";
    private static final String BUNDLE_URL = "bundle_url";
    private static final String PUBLISHED = "published";
    private static final String MAIN_COMPONENT_NAME = "main_component_name";
    private static final String NANOAPP_TYPE = "nanoapp_type";
    private static final String PACKAGE_NAME = "package_name";
    private static final String CURRENT_VERSION_CODE = "current_version_code";
    private static final String LATEST_VERSION_CODE = "latest_version_code";
    private static final String CURRENT_VERSION_NAME = "current_version_name";
    private static final String LATEST_VERSION_NAME = "latest_version_name";
    private static final String LATEST_CHECKSUM = "latest_checksum";
    private static final String CURRENT_CHECKSUM = "current_checksum";
    private static final String CREATED_AT = "created_at";
    private static final String UPDATED_AT = "updated_at";


    private static NanoappsDatabaseHelper sInstance;

    public static synchronized NanoappsDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new NanoappsDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }



    public NanoappsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.w(TAG, "Created the Database");
        String CREATE_NANOAPPS_TABLE = "CREATE TABLE " + TABLE_NANOAPPS +
                "(" +
                ID + " INTEGER PRIMARY KEY," + // Define a primary key
                NAME + " VARCHAR(255), "+
                DESCRIPTION + " VARCHAR(255), "+
                IMAGE_URL + " VARCHAR(255), "+
                BUNDLE_URL + " VARCHAR(255), "+
                PUBLISHED + " BOOLEAN, "+
                MAIN_COMPONENT_NAME + " VARCHAR(100), "+
                NANOAPP_TYPE + " VARCHAR(100), "+
                PACKAGE_NAME + " VARCHAR(100), "+
                CURRENT_VERSION_CODE + " INT, "+
                CURRENT_VERSION_NAME + " VARCHAR(100), "+
                LATEST_VERSION_CODE + " INT, "+
                LATEST_VERSION_NAME + " VARCHAR(100), "+
                CURRENT_CHECKSUM + " VARCHAR(100), "+
                LATEST_CHECKSUM + " VARCHAR(100), "+
                CREATED_AT + " DATETIME, "+
                UPDATED_AT + " DATETIME "+
                ")";
        db.execSQL(CREATE_NANOAPPS_TABLE);
    }

    public boolean addOrUpdateNanoapp(Nanoapp nanoapp) {
        // The database connection is cached so it's not expensive to call getWriteableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        Log.w(TAG, "addOrUpdateNanoapp");
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(NAME, nanoapp.name);
            values.put(DESCRIPTION, nanoapp.description);
            values.put(IMAGE_URL, nanoapp.image_url);
            values.put(BUNDLE_URL, nanoapp.bundle_url);
            values.put(MAIN_COMPONENT_NAME, nanoapp.main_component_name);
            values.put(NANOAPP_TYPE, nanoapp.nanoapp_type);
            values.put(PACKAGE_NAME, nanoapp.package_name);
            values.put(CURRENT_VERSION_CODE, nanoapp.current_version_code);
            values.put(CURRENT_VERSION_NAME, nanoapp.current_version_name);
            values.put(LATEST_VERSION_CODE, nanoapp.latest_version_code);
            values.put(LATEST_VERSION_NAME, nanoapp.latest_version_name);
            values.put(CURRENT_CHECKSUM, nanoapp.current_checksum);
            values.put(LATEST_CHECKSUM, nanoapp.latest_checksum);
            values.put(CREATED_AT, nanoapp.created_at);
            values.put(UPDATED_AT, nanoapp.updated_at);
            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_NANOAPPS, values, PACKAGE_NAME + "= ?", new String[]{nanoapp.package_name});
            // Check if update succeeded
            if (rows != 1) {
                Log.w(TAG, "Row not found so inserting it");
                db.insertOrThrow(TABLE_NANOAPPS, null, values);
                db.setTransactionSuccessful();
            } else {
                Log.d(TAG,"Nanoappp updated");
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add or update user");
            return false;
        } finally {
            db.endTransaction();
        }
        Log.w(TAG, "Insertion successful");
        return true;
    }

    public Nanoapp getNanoapp(String packageName) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        String nanoappsSelectQuery = String.format("SELECT * FROM %s WHERE %s = ?",
                 TABLE_NANOAPPS, PACKAGE_NAME);
        Log.w(TAG, "Querying for the records = "+ nanoappsSelectQuery+ "package name = "+packageName);
        Cursor cursor = db.rawQuery(nanoappsSelectQuery, new String[]{packageName});
        Log.w(TAG, "Querying for the records count = "+ cursor.getCount());
        Nanoapp nanoapp = null;
        try {
            if (cursor.moveToFirst()) {
                Log.w(TAG, "Got the nanoapp I always needed "+ nanoapp);
                nanoapp = new Nanoapp();
                nanoapp.name = cursor.getString(cursor.getColumnIndex(NAME));
                nanoapp.description = cursor.getString(cursor.getColumnIndex(DESCRIPTION));
                nanoapp.image_url = cursor.getString(cursor.getColumnIndex(IMAGE_URL));
                nanoapp.bundle_url = cursor.getString(cursor.getColumnIndex(BUNDLE_URL));
                nanoapp.main_component_name = cursor.getString(cursor.getColumnIndex(MAIN_COMPONENT_NAME));
                nanoapp.nanoapp_type = cursor.getString(cursor.getColumnIndex(NANOAPP_TYPE));
                nanoapp.package_name = cursor.getString(cursor.getColumnIndex(PACKAGE_NAME));
                nanoapp.current_version_code = cursor.getString(cursor.getColumnIndex(CURRENT_VERSION_CODE));
                nanoapp.current_version_name = cursor.getString(cursor.getColumnIndex(CURRENT_VERSION_NAME));
                nanoapp.latest_version_code = cursor.getString(cursor.getColumnIndex(LATEST_VERSION_CODE));
                nanoapp.latest_version_name = cursor.getString(cursor.getColumnIndex(LATEST_VERSION_NAME));
                nanoapp.current_checksum = cursor.getString(cursor.getColumnIndex(CURRENT_CHECKSUM));
                nanoapp.latest_checksum = cursor.getString(cursor.getColumnIndex(LATEST_CHECKSUM));
                nanoapp.created_at = cursor.getString(cursor.getColumnIndex(CREATED_AT));
                nanoapp.updated_at = cursor.getString(cursor.getColumnIndex(UPDATED_AT));
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
            return null;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        Log.w(TAG, "Everything is a success..god bless me");
        return nanoapp;
    }

    public JSONArray getAllPackages() {
        SQLiteDatabase db = getWritableDatabase();
        String nanoappsSelectQuery = String.format("SELECT * FROM nanoapps", TABLE_NANOAPPS);
        Cursor cursor = db.rawQuery(nanoappsSelectQuery, new String[]{});
        JSONArray packageInfoList = new JSONArray();
        try {
            if (cursor.moveToFirst()) {
                do {
                    JSONObject packageInfo = new JSONObject();
                    packageInfo.put(PACKAGE_NAME,
                            cursor.getString(cursor.getColumnIndex(PACKAGE_NAME)));
                    packageInfo.put(CURRENT_VERSION_CODE,
                            cursor.getString(cursor.getColumnIndex(CURRENT_VERSION_CODE)));
                    packageInfoList.put(packageInfo);
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
            return null;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return packageInfoList;
    }



    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NANOAPPS);
            onCreate(db);
        }
    }
}