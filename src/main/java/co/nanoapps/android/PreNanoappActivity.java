package co.nanoapps.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static co.nanoapps.android.Constants.DEVICE_AUTH_TOKEN;
import static co.nanoapps.android.Constants.IS_TYPE_SDK;
import static co.nanoapps.android.Constants.NANOAPP;
import static co.nanoapps.android.Constants.NANOAPPS_ASSETS_URL;
import static co.nanoapps.android.Constants.NANOAPP_ID;
import static co.nanoapps.android.Constants.PACKAGE_NAME;
import static co.nanoapps.android.Constants.X_NANOAPP_DEVICE_TOKEN;
import static co.nanoapps.android.Nanoapps.context;

/**
 * Created by dineshswamy on 10/6/17.
 */

public class PreNanoappActivity extends AppCompatActivity {
    public static final String TAG = "PreNanoappActivity";
    private Nanoapp nanoapp;
    public TextView nanoappStatus;
    public static NanoappsDatabaseHelper nanoappsDatabaseHelper;
    public ReactNativeHost getReactNativeHost(final String fileLocation) {
        final ReactNativeHost defaultReactNativeHost = new ReactNativeHost(context) {
            @Override
            public boolean getUseDeveloperSupport() {
                return false;
            }

            @Override
            protected List<ReactPackage> getPackages() {
                return Nanoapps.appReactPackages;
            }

            @Override
            protected String getJSBundleFile() {
                String location = Environment.getExternalStorageDirectory().getAbsolutePath()+fileLocation;
                Log.w("Using JS bundle", location);
                //return "assets://index.android.js";
                return location;
            }
        };
        return defaultReactNativeHost;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pre_nanoapp_activity);
        nanoappStatus = (TextView)findViewById(R.id.nanoapp_status_txt);
        nanoappsDatabaseHelper = NanoappsDatabaseHelper.getInstance(this.getApplicationContext());
        boolean isTypeSDK = getIntent().getBooleanExtra(IS_TYPE_SDK, false);
        if (isTypeSDK) {
            String packageName = getIntent().getStringExtra(PACKAGE_NAME);
            nanoapp = nanoappsDatabaseHelper.getNanoapp(packageName);
            Log.w(TAG, "Nanoapp not found .. so downloading it" + nanoapp);
            if(nanoapp == null) {
                downloadPackage(packageName);
            } else {
                appLauncher(nanoapp);
            }
        } else {
            nanoapp = getIntent().getParcelableExtra(NANOAPP);
            if (nanoapp.exists()) {
                this.appLauncher(nanoapp);
            } else {
                this.downloadBundle(nanoapp);
            }
        }
    }

    private void downloadPackage(String packageName) {
        String url = Constants.NANOAPPS_API_URL+"/api/nanoapp/get_nanoapp_details?package_name=";
        SharedPreferences _prefs = context.getSharedPreferences(Constants.NANOAPP_PREFERENCES,
                Context.MODE_PRIVATE);
        final String x_nanoapp_device_token = "Bearer "+_prefs.getString(DEVICE_AUTH_TOKEN,"");
        Map<String, String> params = new HashMap();
        params.put(Constants.PACKAGE_NAME, packageName);
        JSONObject parameters = new JSONObject(params);
        url = url + packageName;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Nanoapp sdknanoapp = null;
                try {
                    Log.w(TAG, "Downloading package details complete "+response);
                    sdknanoapp = new Nanoapp(response.getJSONObject("nanoapp"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.w(TAG, e.getLocalizedMessage());
                }
                nanoapp = sdknanoapp;
                downloadBundle(nanoapp);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put(X_NANOAPP_DEVICE_TOKEN,x_nanoapp_device_token);
                return params;
            }
        };
        Log.w(TAG, jsonObjectRequest.getUrl());
        Nanoapps.getRequestQueue().add(jsonObjectRequest);
    }

    private void downloadBundle(final Nanoapp nanoapp) {
        if (nanoapp == null) {
            nanoappStatus.setText("Something went wrong. Please try again");
        }
        Log.w(TAG, "Downloading the bundle");
        String url = NANOAPPS_ASSETS_URL + nanoapp.bundle_url;
        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, url, new Response.Listener<byte[]>() {
            @Override
            public void onResponse(byte[] response) {
                try {
                    if (response != null) {
                        File folder = new File(nanoapp.getAbsoluteFolder());
                        if (!folder.exists()) {
                            folder.mkdirs();
                        }
                        File bundleFile = new File(folder.getAbsolutePath(), "index.android.js");
                        bundleFile.createNewFile();
                        FileOutputStream outputStream;
                        outputStream = new FileOutputStream(bundleFile, false);
                        outputStream.write(response);
                        outputStream.close();
                        //Verify current_checksum of the file downloaded
                        if (Utils.verifyChecksum(bundleFile, nanoapp.current_checksum)){
                            nanoappsDatabaseHelper.addOrUpdateNanoapp(nanoapp);
                            Log.w(TAG, "Downloading the bundle complete");
                            appLauncher(nanoapp);
                        } else {
                            Log.w("Nanoapp", "Checksum Error Occurred");
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {

            }
        }, null);
        RequestQueue mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        mRequestQueue.add(request);
    }

    public void appLauncher(Nanoapp nanoapp) {
        String file = "/nanoapps/"+nanoapp.package_name+"/"+nanoapp.current_version_code+"/index.android.js";
        Nanoapps.mReactNativeHost= getReactNativeHost(file);
        NanoappActivity.current_component_name = nanoapp.main_component_name;
        Intent intent = new Intent(this, NanoappActivity.class);
        intent.putExtra(NANOAPP_ID, nanoapp.id);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
        return;
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
