package co.nanoapps.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;



/**
 * Created by dineshswamy on 9/6/17.
 */

public class Utils {

    public static int getTimeZoneOffset() {
        TimeZone timezone = Calendar.getInstance().getTimeZone();
        int offset = timezone.getRawOffset();

        if (timezone.inDaylightTime(new Date()))
            offset = offset + timezone.getDSTSavings();

        return offset / 1000;
    }

    public static String getCorrectedLanguage() {
        String lang = Locale.getDefault().getLanguage();

        // https://github.com/OneSignal/OneSignal-Android-SDK/issues/64
        if (lang.equals("iw"))
            return "he";
        if (lang.equals("in"))
            return "id";
        if (lang.equals("ji"))
            return "yi";

        // https://github.com/OneSignal/OneSignal-Android-SDK/issues/98
        if (lang.equals("zh"))
            return lang + "-" + Locale.getDefault().getCountry();

        return lang;
    }

    public static String encode(String input) {
        // This is base64 encoding, which is not an encryption
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    public static String decode(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

    public static boolean verifyChecksum(File file, String checksum) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        byte[] dataBytes = new byte[1024];
        int nread = 0;

        try {
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        };

        byte[] mdbytes = md.digest();

        //convert the byte to hex format
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        Log.w("MD5 hash of the file", "Digest(in hex format):: " + sb.toString());
        if (checksum.equals(sb.toString())) {
            return true;
        }
        return false;
    }

    public static String getEncryptedPreference(Context context, String key, String defaultValue) {
        key = Utils.encode(key);
        Log.w("AuthString - r", "key = "+ key);
        SharedPreferences _prefs = context.getSharedPreferences(Constants.NANOAPP_PREFERENCES, Context.MODE_PRIVATE);
        String value = _prefs.getString(key, defaultValue);
        if (!value.equalsIgnoreCase(defaultValue)) {
            value = Utils.decode(value);
        }
        Log.w("AuthString - r", "value = "+ value);
        return value;
    }

    public static String parseDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }

}
